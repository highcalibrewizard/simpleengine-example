package com.highcalibrewizard.examplegame;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;

import com.highcalibrewizard.simpleengine.Animation;
import com.highcalibrewizard.simpleengine.CanvasText;
import com.highcalibrewizard.simpleengine.DisplayFacade;
import com.highcalibrewizard.simpleengine.Entity;
import com.highcalibrewizard.simpleengine.FontLoader;
import com.highcalibrewizard.simpleengine.Game;
import com.highcalibrewizard.simpleengine.GameCanvas;
import com.highcalibrewizard.simpleengine.ImageLoader;
import com.highcalibrewizard.simpleengine.MouseCanvas;
import com.highcalibrewizard.simpleengine.Save;
import com.highcalibrewizard.simpleengine.StreamPlayerWrapper;
import com.highcalibrewizard.simpleengine.Tick;

/**
 * Implement Tick to gain access to working mouse position/click on canvas, and rendering callback
 * @author bandersson
 *
 */
public class Main implements Tick {
	
	private Game area;
	private Entity cat;
	private Entity box;
	private GameCanvas canvas;
	private StreamPlayerWrapper catSnd;
	private StreamPlayerWrapper bubbleSnd;
	private CanvasText titleText;
	private CanvasText fpsCounter;
	private Font titleFont;
	private DisplayFacade display;
	
	//Comment this away for your own project
	public static void main(String[] args) {
		new Main();
	}
	
	public Main() {
		
		/**
		 * The DisplayFacade contains info about screen, and helper methods for scaling
		 */
		display = DisplayFacade.getInstance();

		/**
		 * Game controller. DisplayFacade getWidth/height will return current screen resolution
		 */
		area = new Game(new Dimension(display.getWidth(),display.getHeight()), "My Game", this, Color.WHITE, true);

		/**
		 * Drawing canvas
		 */
		canvas = area.getCanvas();
		/**
		 * Image for cat
		 */
		BufferedImage catImg = ImageLoader.getImage("cat.jpg");
		/**
		 * The "player" Entity. We scale the cat relative to baseline 1920 * 1080. On a screen with greater resolution it
		 * should physically appear the same size as the baseline
		 */
		cat = new Entity(new Rectangle(100,100,100,100), catImg);
		cat.setSpeed(10);
		
		/**
		 * Loading up images for animations
		 */
		List<BufferedImage> boxAniList = ImageLoader.getImagesAsList("frame1.png","frame2.png",
				"frame3.png","frame4.png");
		Animation boxAnimation = new Animation(boxAniList, 10);
		
		/**
		 * Creating the box entity. If will just stand still and loop once
		 */
		box = new Entity(new Rectangle(200,200, 80, 81), null);
		box.setAnimation(boxAnimation);
		
		/**
		 * Will enable animation. Of course you can also have this in logic after GameArea init();
		 */
		box.getAnimation().start();	
		
		
		/**
		 * Text to be drawn on Canvas. The FontLoader is a convenience class for loading in custom fonts supplied with the game
		 */
		titleFont = FontLoader.loadFont(new File("SuperMario256.ttf"), 32, Font.PLAIN);
		titleText = new CanvasText("CatBounce. Press space to switch to scene 2", 50,36, titleFont);
		
		/**
		 * FPS counter
		 */
		Font fpsFont = new Font(null, Font.PLAIN, 32);
		fpsCounter = new CanvasText(""+0, 50,72, fpsFont);
		
		/**
		 * Sound initializations
		 */
		catSnd = new StreamPlayerWrapper(new File("cat.wav"));
		bubbleSnd = new StreamPlayerWrapper(new File("bubble.mp3"));
		
		/**
		 * Grabbing the savehandler.
		 */
		Save saveHandler = Save.getInstance();
		
		/**
		 * Example object holding some settings/saved data
		 */
		ExampleSettings settings = new ExampleSettings();
		File file = new File("settings");
		
		/**
		 * Saving examplesettings
		 */
		saveHandler.toJson(file, settings);
		/**
		 * Loading examplesettings. Need to cast to ExampleSettings, since Object is returned
		 */
		settings = (ExampleSettings) saveHandler.fromJson(file, ExampleSettings.class);
		if (settings != null)	//handle if file is non existant
			System.out.println(settings.toString());

		/**
		 * Run this last, it starts the ticking and drawing
		 */
		area.init();
		
	}
	
	/**
	 * Handles all the updating and drawing.
	 */
	@Override
	public void tick(Graphics g) {
		/**
		 * Collisions, movement
		 */
		detectCollisions();
		cat.move();
		box.move();
		slowDownCat();
		
		
		/**
		 * Drawing. Notice we must clear the canvas on each drawing pass
		 */
		canvas.clear(g);
		canvas.drawEntity(cat, g);
		
		/**
		 * Use this method for drawing animations. This section will cause the animation to loop once and then stop on the first frame.
		 * It is recommended that you put all your entities in an array and loop through them: check if animation is not null, 
		 * and then do appropriate logic
		 */
		if (box.getAnimation().ifEnabled() && box.getAnimation().isPlaying())
			canvas.drawAnimationImage(box.getAnimation().serveImage(), box, g);
		else if (box.getAnimation().ifEnabled() && !box.getAnimation().isPlaying()) {
			/**
			 * After looping the first frame will display. We use the overloaded serveImage to force this
			 */
			canvas.drawAnimationImage(box.getAnimation().serveImage(0), box, g);
		}
		
		canvas.drawText(titleText, g);
		fpsCounter.setText(Long.toString(Game.getFPS()));
		canvas.drawText(fpsCounter, g);
	}
	
	public void slowDownCat() {
		int xS = cat.getXSpeed();
		int yS = cat.getYSpeed();

		if (xS > 0) {
			cat.setXSpeed(--xS);
		} else if (xS < 0) {
			cat.setXSpeed(++xS);
		}
		if (yS > 0) {
			cat.setYSpeed(--yS);
		} else if (yS < 0) {
			cat.setYSpeed(++yS);
		}
	}
	
	public void detectCollisions() {
		detectBorderCollisions();
		//TODO add other collision stuff here
	}
	
	/**
	 * Big blog of example logic
	 */
	public void detectBorderCollisions() {
		if ((cat.getRect().x + cat.getRect().width) >= canvas.getWidth()) {
			cat.getRect().x = canvas.getWidth()-1-cat.getRect().width;
			cat.setXSpeed(-cat.getSpeed());
			catSnd.play();
		} else if (cat.getRect().x <= 0) {
			cat.setXSpeed(cat.getSpeed());
			cat.getRect().x = 1;
			catSnd.play();
		}
		if (cat.getRect().y <= 0) {
			cat.getRect().y = 1;
			cat.setYSpeed(cat.getSpeed());
			catSnd.play();
		} else if (cat.getRect().y + cat.getRect().height >= canvas.getHeight()) {
			cat.getRect().y = canvas.getHeight()-1-cat.getRect().height;
			cat.setYSpeed(-cat.getSpeed());
			catSnd.play();
		}
	}

	@Override
	public void mousePosition(Point p) {

	}

	@Override
	public void mouseClick(MouseEvent e) {
		if (cat.detectCollision(e.getPoint())) {
			System.out.println("clicked kitty");
			bubbleSnd.play();
		}
	}

	@Override
	public void keyPressed(int key) {
		if (key == KeyEvent.VK_SPACE) {
			/**
			 * Loads in a new drawing context. A suggestion is to pass in a class with references to shared resources that are to be drawn
			 * by different contexts
			 */
			Scene2 next = new Scene2(area, this);
			area.setContext(next);
		} else if (key == KeyEvent.VK_UP) {
			cat.setYSpeed(-cat.getSpeed());
		} else if (key == KeyEvent.VK_DOWN) {
			cat.setYSpeed(cat.getSpeed());
		} else if (key == KeyEvent.VK_LEFT) {
			cat.setXSpeed(-cat.getSpeed());
		} else if (key == KeyEvent.VK_RIGHT) {
			cat.setXSpeed(cat.getSpeed());
		}
		
	}

}