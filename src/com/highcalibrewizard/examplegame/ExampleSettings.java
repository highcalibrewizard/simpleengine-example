package com.highcalibrewizard.examplegame;

/**
 * Example settings file
 * @author bandersson
 *
 */
public class ExampleSettings {
	private String setting = "This is a saved setting";
	
	public void setSetting(String setting) {
		this.setting = setting;
	}
	
	public String getSetting() {
		return setting;
	}
	
	@Override
	public String toString() {
		return setting;
	}
}
