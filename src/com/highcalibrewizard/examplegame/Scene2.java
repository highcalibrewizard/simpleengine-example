package com.highcalibrewizard.examplegame;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import com.highcalibrewizard.simpleengine.CanvasText;
import com.highcalibrewizard.simpleengine.DisplayFacade;
import com.highcalibrewizard.simpleengine.Game;
import com.highcalibrewizard.simpleengine.GameCanvas;
import com.highcalibrewizard.simpleengine.Tick;

public class Scene2 implements Tick {
	
	private CanvasText titleText;
	private GameCanvas canvas;
	private Game game;
	private Main previous;
	private DisplayFacade display;
	
	public Scene2(Game game, Main previousScene) {
		
		/**
		 * Saving reference to the old scene for switching
		 */
		previous = previousScene;
		
		/**
		 * Saving reference to the game.
		 */
		this.game = game;
		this.canvas = game.getCanvas();
		
		/**
		 * Text to be drawn on Canvas
		 */
		Font titleFont = new Font("Arial", Font.PLAIN,36);
		titleText = new CanvasText("Scene 2. Press space to switch back", 50,36, titleFont);
		display = DisplayFacade.getInstance();
	}

	@Override
	public void tick(Graphics g) {
		g.clearRect(0, 0, display.getWidth(), display.getHeight());
		g.drawString(titleText.getText(), titleText.getX(), titleText.getY());

	}

	@Override
	public void mousePosition(Point p) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseClick(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(int key) {
		if (key == KeyEvent.VK_SPACE) {
			/**
			 * This will switch back to the old scene
			 */
			game.setContext(previous);
		}
		
	}

}
